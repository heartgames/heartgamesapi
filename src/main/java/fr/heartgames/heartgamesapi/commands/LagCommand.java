package fr.heartgames.heartgamesapi.commands;

import fr.heartgames.heartgamesapi.api.GameAPI;
import fr.heartgames.heartgamesapi.api.players.HeartGamesPlayer;
import fr.heartgames.heartgamesapi.utils.AbstractCommand;
import fr.heartgames.heartgamesapi.utils.general.TimeUtils;
import net.minecraft.server.v1_8_R3.MinecraftServer;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 * Created by Toinetoine1 on 28/09/2019.
 */

public class LagCommand extends AbstractCommand {

    DateFormat dateFormat;

    public LagCommand() {
        super("lag", HeartGamesPlayer.GamePermission.PLAYER);
        this.allowConsole(false);
        dateFormat = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
    }

    @Override
    public void executeCommand(CommandSender sender, String[] args) {
        HeartGamesPlayer player = (HeartGamesPlayer) sender;

        player.sendMessage(" ");
        player.sendMessage("§aInstance §e" + GameAPI.getAPI().getGameServer().getServerName() + " §aà " + dateFormat.format(TimeUtils.getTime()));
        player.sendMessage("§7Les TPS actuelles sont: §a" + getTPS());
        player.sendMessage("§7Vous avez un ping de: §a" + player.getPing());
        player.sendMessage(" ");
    }

    private String getTPS() {
        StringBuilder sb = new StringBuilder();
        for (double tps : MinecraftServer.getServer().recentTps) {
            sb.append(format(tps));
            sb.append(", ");
        }

        return sb.substring(0, sb.length() - 2);
    }

    private String format(double tps) {
        return ((tps > 18.0) ? ChatColor.GREEN : (tps > 16.0) ? ChatColor.YELLOW : ChatColor.RED).toString()
                + ((tps > 20.0) ? "*" : "") + Math.min(Math.round(tps * 100.0) / 100.0, 20.0);
    }

}
