package fr.heartgames.heartgamesapi.servers.packets;

import fr.heartgames.heartgamesapi.tech.redis.messaging.Packet;
import lombok.Getter;

/**
 * Created by Toinetoine1 on 29/10/2019.
 */

public abstract class PacketServerName extends Packet {

    public PacketServerName() {
    }

    public static class Request extends PacketServerName{
        public static final String PACKET_TAG = "PACKETSERVERNAME_REQUEST";

        @Getter
        private int port;

        public Request() {}

        public Request(int port) {
            this.port = port;
        }

        @Override
        public void read(String[] data) {
            this.port = Integer.parseInt(data[0]);
        }

        @Override
        public String[] write() {
            return new String[]{port+""};
        }
    }

    public static class Response extends PacketServerName{
        public static final String PACKET_TAG = "PACKETSERVERNAME_RESPONSE";

        @Getter
        private int port;
        @Getter
        private String serverName;

        public Response() {}

        public Response(int port, String serverName) {
            this.port = port;
            this.serverName = serverName;
        }

        @Override
        public void read(String[] data) {
            this.port = Integer.parseInt(data[0]);
            this.serverName = data[1];
        }

        @Override
        public String[] write() {
            return new String[]{port+"", serverName};
        }
    }
}
