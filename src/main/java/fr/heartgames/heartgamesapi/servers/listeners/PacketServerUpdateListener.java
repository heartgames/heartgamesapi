package fr.heartgames.heartgamesapi.servers.listeners;

import fr.heartgames.heartgamesapi.GamePlugin;
import fr.heartgames.heartgamesapi.api.GameAPI;
import fr.heartgames.heartgamesapi.servers.AutoUpdater;
import fr.heartgames.heartgamesapi.servers.packets.PacketServerUpdate;
import fr.heartgames.heartgamesapi.tech.redis.messaging.PacketHandler;
import fr.heartgames.heartgamesapi.tech.redis.messaging.PacketListener;

/**
 * Created by Toinetoine1 on 19/10/2019.
 */

public class PacketServerUpdateListener implements PacketListener {

    @PacketHandler
    public void onReceive(PacketServerUpdate packet){
        if(packet.getServerName().equalsIgnoreCase(GameAPI.getAPI().getGameServer().getServerName()) || packet.getServerName().equalsIgnoreCase("all")){
            System.out.println("Trying to update this server..");
            new AutoUpdater(GameAPI.getAPI(), GamePlugin.UPDATE_API_URL, true);
        }

    }

}
