package fr.heartgames.heartgamesapi.permissions;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.mongodb.BasicDBObject;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Filters;
import fr.heartgames.heartgamesapi.api.GameAPI;
import fr.heartgames.heartgamesapi.permissions.groups.Group;
import fr.heartgames.heartgamesapi.run.RunType;
import fr.heartgames.heartgamesapi.tech.mongodb.MongoFactory;
import lombok.Getter;
import org.bson.Document;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Toinetoine1 on 05/08/2019.
 */

public class PermissionManager {

    @Getter
    private static PermissionManager instance;

    private MongoCollection<Document> collection;
    @Getter
    private Map<String, Group> groups;

    public PermissionManager() {
        instance = this;
        groups = new HashMap<>();
        collection = MongoFactory.getInstance().getMainDatabase().getCollection("groups");

        reload();
    }


    public void reload() {
        groups.clear();

        Document document = collection.find(new Document("_id", RunType.PROD.name())).first();

        if (document == null) {
            for (Rank rank : Rank.values()) {
                groups.put(rank.name(), new Group(rank.getPower(), new ArrayList<>(Arrays.asList("null")), new ArrayList<>(Arrays.asList("null"))));
            }

            insertIntoMongo();
        } else {
            boolean needToPush = false;
            for (Rank rank : Rank.values()) {
                if (document.containsKey(rank.name())) {
                    JsonObject jsonObject = new JsonParser().parse(document.toJson()).getAsJsonObject();

                    groups.put(rank.name(), new Group(jsonObject.getAsJsonObject(rank.name())));
                } else {
                    groups.put(rank.name(), new Group(rank.getPower(), new ArrayList<>(Arrays.asList("null")), new ArrayList<>(Arrays.asList("null"))));
                    needToPush = true;
                }
            }

            if (needToPush) {
                insertIntoMongo();
            }

        }

        for (Map.Entry<String, Group> entry : groups.entrySet()) {
            Group mainGroup = entry.getValue();

            LinkedList<Group> stack = new LinkedList<>();
            ArrayList<Group> alreadyVisited = new ArrayList<>();
            stack.push(mainGroup);
            alreadyVisited.add(mainGroup);
            List<String> allPerm = new ArrayList<>(mainGroup.getPermissions());

            while (!stack.isEmpty()) {
                Group now = stack.pop();
                for (String sonName : now.getInheritance()) {
                    Group son = instance.getGroups().get(sonName);
                    if (son != null && !alreadyVisited.contains(son)) {
                        stack.push(son);
                        alreadyVisited.add(son);
                        allPerm.addAll(son.getPermissions());
                    }
                }
            }

            mainGroup.setAllPerms(allPerm);
            mainGroup.setLinkedTo(Rank.valueOf(entry.getKey()));
            System.out.println("group: " + mainGroup.getPower() + " alreadyVisited:" + alreadyVisited.stream().map(Group::getPower).collect(Collectors.toList()));
        }
    }

    private static void insertIntoMongo() {
        List<String> sortedRank = instance.getGroups().entrySet().stream().sorted(Comparator.comparingInt(value -> value.getValue().getPower())).map(Map.Entry::getKey).collect(Collectors.toList());

        Document newDoc = new Document();
        newDoc.put("_id", GameAPI.getAPI().getRunType().name());
        for (String loop : sortedRank) {
            newDoc.append(loop, instance.getGroups().get(loop));
        }

        instance.collection.insertOne(newDoc);
    }

    public static void push(Group group, String perm) {
        Document operation = new Document("$push", new BasicDBObject(group.getLinkedTo().name() + ".permissions", perm));
        instance.collection.findOneAndUpdate(Filters.eq("_id", RunType.PROD.name()), operation);
    }

    public static void pull(Group group, String perm) {
        Document operation = new Document("$pull", new BasicDBObject(group.getLinkedTo().name() + ".permissions", perm));
        instance.collection.findOneAndUpdate(Filters.eq("_id", RunType.PROD.name()), operation);
    }

    public static Group getGroup(Rank rank) {
        return getGroup(rank.name());
    }

    public static Group getGroup(String rankName) {
        return instance.getGroups().getOrDefault(rankName, null);
    }

    public static int getPower(Rank rank) {
        return getGroup(rank).getPower();
    }
}
