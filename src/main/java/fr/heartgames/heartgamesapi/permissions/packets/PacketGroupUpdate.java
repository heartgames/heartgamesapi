package fr.heartgames.heartgamesapi.permissions.packets;

import fr.heartgames.heartgamesapi.tech.redis.messaging.Packet;
import lombok.Getter;


/**
 * Created by Toinetoine1 on 26/09/2019.
 */


public class PacketGroupUpdate extends Packet {

    public static final String PACKET_TAG = "PACKETGROUPUPDATE";

    @Getter
    private String rankName;
    @Getter
    private String perm;
    @Getter
    private Type type;


    public PacketGroupUpdate() {
    }

    public PacketGroupUpdate(String rankName, String perm, Type type) {
        this.rankName = rankName;
        this.perm = perm;
        this.type = type;
    }

    public PacketGroupUpdate(Type type) {
        this.type = type;
        this.rankName = "";
        this.perm = "";
    }

    @Override
    public void read(String[] data) {
        this.rankName = data[0];
        this.perm = data[1];
        this.type = Type.valueOf(data[3]);
    }

    @Override
    public String[] write() {
        return new String[]{rankName, perm, type.name()};
    }

    public enum Type{
        ADD, REMOVE, RELOAD
    }
}
