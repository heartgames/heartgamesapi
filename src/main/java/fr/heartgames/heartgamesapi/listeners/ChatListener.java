package fr.heartgames.heartgamesapi.listeners;

import fr.heartgames.heartgamesapi.api.players.HeartGamesPlayer;
import fr.heartgames.heartgamesapi.permissions.PermissionManager;
import fr.heartgames.heartgamesapi.permissions.Rank;
import fr.heartgames.heartgamesapi.utils.HeartListener;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.AsyncPlayerChatEvent;

/**
 * Created by Toinetoine1 on 19/08/2019.
 */

public class ChatListener extends HeartListener {

    public static boolean enabled = false;

    @EventHandler
    public void onChat(AsyncPlayerChatEvent event){
        HeartGamesPlayer player = (HeartGamesPlayer) event.getPlayer();

        if(enabled){
            String message = event.getMessage();
            int playerPower = player.getMainGroup().getPower();
            boolean isStaff = playerPower >= PermissionManager.getPower(Rank.HELPER);

            char tag = '7';
            if(isStaff){
                message = ChatColor.translateAlternateColorCodes('&', message);
                tag = 'f';
            }

            event.setFormat(player.getPlayerData().getMainRank().getFormattedName()+player.getName()+" » §"+tag+message);
        }
    }

}
