package fr.heartgames.heartgamesapi.listeners;

import fr.heartgames.heartgamesapi.api.players.HeartGamesPlayer;
import fr.heartgames.heartgamesapi.utils.HeartListener;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerQuitEvent;

/**
 * Created by Toinetoine1 on 16/08/2019.
 */

public class QuitListener extends HeartListener {

    @EventHandler(priority = EventPriority.MONITOR)
    public void onQuit(PlayerQuitEvent event){
        HeartGamesPlayer heartGamesPlayer = (HeartGamesPlayer) event.getPlayer();

        heartGamesPlayer.unloadData();
    }

}
