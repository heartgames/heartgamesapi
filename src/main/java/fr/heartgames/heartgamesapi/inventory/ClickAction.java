package fr.heartgames.heartgamesapi.inventory;

/*
    Created by Toinetoine1 on 25/04/2019
*/

import fr.heartgames.heartgamesapi.api.players.HeartGamesPlayer;
import org.bukkit.inventory.ItemStack;

public interface ClickAction {

    void execute(HeartGamesPlayer player, ItemStack clickedItem);

}
