package fr.heartgames.heartgamesapi;

import fr.heartgames.heartgamesapi.api.GameAPI;
import fr.heartgames.heartgamesapi.run.RunType;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Created by Toinetoine1 on 03/08/2019.
 */


public abstract class HeartGamesPlugin extends JavaPlugin {

    public GameAPI getAPI() {
        return GameAPI.getAPI();
    }

    @Override
    public void onEnable() {
        onEnable(getAPI().getRunType());
    }

    public abstract void onEnable(RunType runType);

}
