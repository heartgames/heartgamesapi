package fr.heartgames.heartgamesapi.tech.redis;

import fr.heartgames.heartgamesapi.tech.redis.connection.RedisAccess;
import fr.heartgames.heartgamesapi.tech.redis.messaging.PubSubManager;
import fr.heartgames.heartgamesapi.tech.redis.messaging.Redis;
import lombok.AccessLevel;
import lombok.Getter;
import org.redisson.api.RedissonClient;

import java.util.ArrayList;

/**
 * Created by Toinetoine1 on 04/08/2019.
 */

public class RedisFactory {

    public static final String MAIN_CHANNEL = "HEARTGAMES";

    @Getter
    private final PubSubManager messagingManager;

    @Getter(AccessLevel.PUBLIC)
    private static ArrayList<String> channelsRegistered = new ArrayList<>();
    private static RedisFactory instance;
    private RedisAccess redisAccess;

    public RedisFactory() {
        messagingManager = new PubSubManager();

        Redis.registerChannel(MAIN_CHANNEL);
    }

    public static RedisFactory getInstance() {
        if (instance == null) {
            instance = new RedisFactory();
        }
        return instance;
    }

    public static void subscribeChannel(String channel) {
        new Thread(() -> {
            try {
                RedisFactory.getInstance().getRedisClient().getTopic(channel).addListener(RedisFactory.getInstance().getMessagingManager());
            } catch (Exception e) {
                e.printStackTrace();
            }

        }).start();
    }

    public void setRedisAccess(RedisAccess redisAccess) {
        this.redisAccess = redisAccess;
    }

    public RedissonClient getRedisClient(){

        return redisAccess.getRedissonClient();
    }

}
