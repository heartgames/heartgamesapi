package fr.heartgames.heartgamesapi.tech.redis.messaging.packets;

import fr.heartgames.heartgamesapi.tech.redis.messaging.Packet;

/**
 * Created by Toinetoine1 on 04/08/2019.
 */


public class ExemplePacket extends Packet {

    public static final String PACKET_TAG = "ExemplePacket";

    /*
     IMPORTANT: Un constructeur sans parametres DOIT etre présent dans chaque classe héritant @Packet
     */
    public ExemplePacket() {
    }

    @Override
    public void read(String[] data) {
        System.out.println(data.toString());
    }

    @Override
    public String[] write() {
        return new String[]{"bite", "broute", "binouze"};
    }


}
