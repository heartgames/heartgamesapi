package fr.heartgames.heartgamesapi.tech.redis.messaging;

public enum Target {
    BUNGEE,
    BUKKIT,
    ALL
}
