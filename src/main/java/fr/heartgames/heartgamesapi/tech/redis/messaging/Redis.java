package fr.heartgames.heartgamesapi.tech.redis.messaging;

import fr.heartgames.heartgamesapi.tech.redis.RedisFactory;
import fr.heartgames.heartgamesapi.utils.Utils;
import lombok.Getter;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;

public class Redis {

    @Getter
    private static final HashMap<String, Class<? extends Packet>> packetsDirectory = new HashMap<>();
    @Getter
    private static final HashMap<Class<? extends Packet>, String> reversePacketsDirectory = new HashMap<>();
    private static final HashMap<Class, ArrayList<RegisteredPacketListener>> packetListeners = new HashMap<>();

    /**
     * Use to send a packet throught redis messaging service.
     *
     * @param packet to send.
     */
    public static void publish(String channel, Packet packet) {
        if (RedisFactory.getChannelsRegistered().contains(channel)) {
            if (reversePacketsDirectory.containsKey(packet.getClass())
                    && packetsDirectory.containsValue(packet.getClass())) {
                RedisFactory.getInstance().getMessagingManager().publish(channel, packet);
            } else {
                Utils.severe("Unable to send an unregistered packet.");
            }
        } else {
            Utils.severe("Unable to send a packet on an unregistered channel.");
        }
    }

    public static void publishOnMainChannel(Packet packet) {
        Redis.publish(RedisFactory.MAIN_CHANNEL, packet);
    }

    public static void registerChannel(String channel) {
        if (!RedisFactory.getChannelsRegistered().contains(channel)) {
            RedisFactory.getChannelsRegistered().add(channel);
            RedisFactory.subscribeChannel(channel);
        } else {
            Utils.severe("Unable to register a channel already register.");
        }
    }

    /**
     * Use to register a listeners to a packet.
     *
     * @param packetListener packet listeners
     */
    public static void registerPacketReceiver(PacketListener packetListener) {
        for (Method method : packetListener.getClass().getMethods()) {
            if (method.isAnnotationPresent(PacketHandler.class)) {

                if (method.getParameterCount() != 1) {
                    Utils.severe("Unable to register class " + packetListener.getClass() + " as PacketListener because listener method must have only one argument.");
                    return;
                }

                for (Class clazz : method.getParameterTypes()) {
                    if (!Packet.class.isAssignableFrom(clazz)) {
                        Utils.severe("Unable to register class " + packetListener.getClass() + " as PacketListener because one of the arguments is not a packet.");
                        return;
                    }
                }
                ArrayList<RegisteredPacketListener> registeredPacketListeners = (Redis.packetListeners.get(method.getParameterTypes()[0]) != null ? Redis.packetListeners.get(method.getParameterTypes()[0]) : new ArrayList<>());
                registeredPacketListeners.add(new RegisteredPacketListener(method, method.getParameterTypes()[0], packetListener));
                Redis.packetListeners.put(method.getParameterTypes()[0], registeredPacketListeners);
            }
        }
    }

    @SuppressWarnings("unchecked")
    public static void registerPacketClassInDirectory(String packetTag, Class<? extends Packet> clazz) {
        if (!getReversePacketsDirectory().containsKey(clazz) && !getPacketsDirectory().containsKey(packetTag)) {
            if (Packet.class.isAssignableFrom(clazz)) {
                packetsDirectory.put(packetTag, clazz);
                reversePacketsDirectory.put(clazz, packetTag);
            } else {
                Utils.severe("Error when trying to register packet in directory : " + clazz + ". Class has not Packet as SuperClass.");
            }
        } else {
            Utils.severe("Unable to register class " + clazz + " as Packet. PacketTag or Class already registered.");
        }
    }

    static void received(Packet packet) {
        ArrayList<RegisteredPacketListener> registeredPacketListeners = Redis.packetListeners.get(packet.getClass());
        if(registeredPacketListeners != null) {
            registeredPacketListeners.sort(Comparator.comparingInt(o -> o.getMethod().getAnnotation(PacketHandler.class).value().getSlot()));
            for (RegisteredPacketListener registeredPacketListener : registeredPacketListeners) {
                try {
                    registeredPacketListener.getMethod().invoke(registeredPacketListener.getPacketListener(), packet);
                } catch (IllegalAccessException | InvocationTargetException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    static class RegisteredPacketListener {
        @Getter
        private final Method method;
        @Getter
        private final Class packetClass;
        @Getter
        private final PacketListener packetListener;

        RegisteredPacketListener(Method method, Class packetClass, PacketListener packetListener) {
            this.method = method;
            this.packetClass = packetClass;
            this.packetListener = packetListener;
        }
    }
}
