package fr.heartgames.heartgamesapi.tech.sql;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import fr.heartgames.heartgamesapi.jsonconfig.DatabaseCredentials;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * Created by Toinetoine1 on 31/03/2019.
 */

public class DatabaseAccess {

    private static DatabaseAccess INSTANCE;
    private DatabaseCredentials credentials;
    private HikariDataSource hikariDataSource;

    public DatabaseAccess(DatabaseCredentials credentials) {
        this.credentials = credentials;
        INSTANCE = this;
    }

    public static void init(DatabaseCredentials databaseCredentials) {
        new DatabaseAccess(databaseCredentials).initPool();
    }

    public static void close(){
        INSTANCE.closePool();
    }

    private void setupHikariCP() {
        final HikariConfig hikariConfig = new HikariConfig();

        hikariConfig.setMaximumPoolSize(10);
        hikariConfig.setJdbcUrl(credentials.toURL());
        hikariConfig.setUsername(credentials.getUser());
        hikariConfig.setPassword(credentials.getPass());
        hikariConfig.setMaxLifetime(600000);
        hikariConfig.setIdleTimeout(300000);
        hikariConfig.setLeakDetectionThreshold(300000);
        hikariConfig.setConnectionTimeout(10000);

        this.hikariDataSource = new HikariDataSource(hikariConfig);
    }

    private void initPool() {
        setupHikariCP();
    }

    private void closePool() {
        this.hikariDataSource.close();
    }

    public Connection getConnection() throws SQLException {
        if (this.hikariDataSource == null) {
            System.out.println("Not connected");
            setupHikariCP();
        }

        return this.hikariDataSource.getConnection();
    }

    public static DatabaseAccess getInstance() {
        return INSTANCE;
    }
}
