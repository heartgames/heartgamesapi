package fr.heartgames.heartgamesapi.tech;

import fr.heartgames.heartgamesapi.api.players.data.PlayerData;
import fr.heartgames.heartgamesapi.players.data.GamePlayerData;
import fr.heartgames.heartgamesapi.tech.mongodb.MongoFactory;
import fr.heartgames.heartgamesapi.tech.redis.SaveTag;
import fr.heartgames.heartgamesapi.tech.redis.datastorage.RedisDataHandler;
import fr.heartgames.heartgamesapi.utils.general.GsonUtils;
import org.bson.Document;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.HashMap;

/**
 * Created by Toinetoine1 on 04/08/2019.
 */

public class DataManager {

    public static PlayerData loadProfile(String name) {
        PlayerData redisData = loadFromRedis(name);
        if (redisData == null) {
            PlayerData mongoData = loadFromMongo(name);
            if (mongoData == null) {
                System.out.println("data not found");
                return null;
            }

            return mongoData;
        } else{
            System.out.println("loaded from redis");
            return redisData;
        }
    }

    public static PlayerData loadFromRedis(String name) {
        if (RedisDataHandler.containsUser(SaveTag.getDefaultSaveTag().getRedisTag(), name)) {
            PlayerData playerData = null;
            String data = RedisDataHandler.getUserData(name);
            System.out.println("load from redis data:"+ data);

            playerData = GsonUtils.getCompactGson().fromJson(data, GamePlayerData.class);
            return playerData;
        }

        System.out.println("return null 1");
        return null;
    }

    public static void sendDataToRedis(PlayerData playerData) {
        String serialized = GsonUtils.getCompactGson().toJson(playerData);

        RedisDataHandler.setUserData(playerData.getName(), serialized);
    }

    public static void unloadFromRedis(PlayerData redisData) {
        RedisDataHandler.deleteUser(redisData.getName());
    }

    private static PlayerData loadFromMongo(String name){
        PlayerData playerData = MongoFactory.getInstance().getPlayerCollection().find(new Document("name", name), GamePlayerData.class).first();
        System.out.println("loaded from Mongo: "+GsonUtils.getGson().toJson(playerData));
        return playerData;
    }

    public static void saveToMongo(PlayerData playerData){
        System.out.println(GsonUtils.getGson().toJson(playerData));

        MongoFactory.getInstance().getPlayerCollection().insertOne(playerData);
        /*Document document = new Document()
                .append("realName", playerData.getRealName())
                .append("fakeName", playerData.getName())
                .append("heartcoins", playerData.getHeartCoins())
                .append("mainrank", playerData.getMainRank().name());

        MongoAccess.getInstance().getPlayers().insertOne(document);*/
    }

    public static void updateToMongo(PlayerData playerData){
        if(playerData == null)
            return;

        PlayerData founded = MongoFactory.getInstance().getPlayerCollection().find(new Document("name", playerData.getName())).first();
        System.out.println("founded: "+founded);

        if(founded == null){
            System.out.println("Enable to find data with name: "+playerData.getName());
            return;
        }

        HashMap<String, Object> map = new HashMap<>();
        Field[] fields = playerData.getClass().getDeclaredFields();
        for(Field field : fields){
            if(Modifier.isPrivate(field.getModifiers())){
                try {
                    field.setAccessible(true);
                    map.put(field.getName(), field.get(playerData));
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }

        System.out.println("map update mongo"+map.toString());
        Document value = new Document(map);
        Document operation = new Document("$set", value);
        MongoFactory.getInstance().getPlayerCollection().updateOne(new Document("name", playerData.getName()), operation);

    }
}
