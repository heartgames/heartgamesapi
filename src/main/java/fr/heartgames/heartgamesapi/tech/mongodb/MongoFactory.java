package fr.heartgames.heartgamesapi.tech.mongodb;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import fr.heartgames.heartgamesapi.api.players.data.PlayerData;
import fr.heartgames.heartgamesapi.players.data.GamePlayerData;
import fr.heartgames.heartgamesapi.tech.mongodb.codec.RankEnumCodec;
import fr.heartgames.heartgamesapi.tech.mongodb.connection.MongoAccess;
import lombok.Setter;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.ClassModel;
import org.bson.codecs.pojo.Conventions;
import org.bson.codecs.pojo.PojoCodecProvider;

import java.util.ArrayList;
import java.util.Arrays;

import static com.mongodb.MongoClient.getDefaultCodecRegistry;
import static org.bson.codecs.configuration.CodecRegistries.*;

/**
 * Created by Toinetoine1 on 11/08/2019.
 */

public class MongoFactory {

    private static MongoFactory instance;
    @Setter
    private static MongoAccess mongoAccess;

    private final CodecRegistry pojoCodecRegistry;
    private MongoClient mongoClient;
    private MongoDatabase mainDatabase;
    private MongoCollection<PlayerData> playerDataMongoCollection;

    private MongoFactory() {
        ClassModel<PlayerData> playerDataClassModel = ClassModel.builder(PlayerData.class).enableDiscriminator(true).build();
        ClassModel<GamePlayerData> gamePlayerDataClassModel = ClassModel.builder(GamePlayerData.class).enableDiscriminator(true).build();

        pojoCodecRegistry = fromRegistries(getDefaultCodecRegistry(), fromCodecs(new RankEnumCodec()),
                fromProviders(PojoCodecProvider.builder().automatic(true).register(playerDataClassModel, gamePlayerDataClassModel).conventions(new ArrayList<>(Arrays.asList(Conventions.ANNOTATION_CONVENTION, Conventions.SET_PRIVATE_FIELDS_CONVENTION))).build()));

        mongoClient = mongoAccess.getMongoClient();
        mainDatabase = mongoClient.getDatabase("admin").withCodecRegistry(pojoCodecRegistry);
        playerDataMongoCollection = mainDatabase.getCollection("players", PlayerData.class);
    }

    public MongoClient getMongoClient() {
        return mongoClient;
    }

    public MongoDatabase getMainDatabase() {
        return mainDatabase;
    }

    public MongoCollection<PlayerData> getPlayerCollection() {
        return playerDataMongoCollection;
    }


    public static MongoFactory getInstance() {
        if (instance == null) {
            instance = new MongoFactory();
        }
        return instance;
    }
}
