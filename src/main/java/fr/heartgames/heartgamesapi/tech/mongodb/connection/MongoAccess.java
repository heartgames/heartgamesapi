package fr.heartgames.heartgamesapi.tech.mongodb.connection;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import fr.heartgames.heartgamesapi.jsonconfig.MongoCredentials;
import fr.heartgames.heartgamesapi.tech.mongodb.MongoFactory;

/**
 * Created by Toinetoine1 on 10/08/2019.
 */


public class MongoAccess {

    private static MongoAccess INSTANCE;
    private MongoClient client;

    private MongoAccess(MongoCredentials credentials) {
        INSTANCE = this;
        this.client = MongoClients.create(credentials.toURL());
    }

    public static void init(MongoCredentials credentials){
        MongoAccess mongoAccess = new MongoAccess(credentials);
        MongoFactory.setMongoAccess(mongoAccess);
    }

    public static void close(){
        MongoAccess.getInstance().getMongoClient().close();
    }

    public MongoClient getMongoClient() {
        return client;
    }

    public static MongoAccess getInstance() {
        return INSTANCE;
    }

}
