package fr.heartgames.heartgamesapi.utils;

import fr.heartgames.heartgamesapi.api.players.HeartGamesPlayer;
import fr.heartgames.heartgamesapi.utils.reflections.Reflector;
import lombok.Getter;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandMap;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Getter
public abstract class AbstractCommand implements TabExecutor {

    private static final int MAX_TAB_RETURN = 20;

    private String command;
    private String lobbyPermission;

    private String[] aliases;

    private boolean allowConsole = true;

    public AbstractCommand(String command, HeartGamesPlayer.GamePermission perm, String... aliases) {
        this(command, perm.getPermission(), aliases);
    }

    public AbstractCommand(String command, String permission, String... aliases) {
        this.command = command;
        this.lobbyPermission = permission;
        this.aliases = aliases;

        ReflectCommand result = new ReflectCommand(command);

        result.setAliases(Arrays.asList(aliases));
        result.setExecutor(this);

        getCommandMap().register("gameapi", result);
    }

    public void allowConsole(boolean console) {
        this.allowConsole = console;
    }

    public abstract void executeCommand(CommandSender sender, String[] args);

    @Override
    public final boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!allowConsole && !(sender instanceof Player))
            sender.sendMessage(ChatColor.RED + "This command is only for players.");
        else if (hasPermission(sender)) {
            executeCommand(sender, args);
        } else sender.sendMessage("§cVous n'avez pas les permissions nécessaires");
        return true;
    }

    public boolean hasPermission(CommandSender sender){
        String permission = lobbyPermission;

        return permission == null || permission.isEmpty() || sender.hasPermission(permission);
    }

    private final CommandMap getCommandMap() {
        try {
            return (CommandMap) new Reflector(Bukkit.getServer()).getFieldValue("commandMap");
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public Collection<String> doTab(CommandSender sender, String[] args){
        return Bukkit.getOnlinePlayers().stream().map(HumanEntity::getName).collect(Collectors.toList());
    }

    public String[] changeArgs(CommandSender sender, String[] args){
        return args;
    }

    @Override
    public final List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
        if (!allowConsole && !(sender instanceof Player))
            sender.sendMessage(ChatColor.RED + "This command is only for players.");
        else if (hasPermission(sender)) {
            args = changeArgs(sender, args);
            String searched = (args.length == 0 ? "" : args[args.length - 1]).toLowerCase();

            Collection<String> result = doTab(sender, args);

            if(result != null)
                return doTab(sender, args).stream().filter(arg ->
                        searched.isEmpty() || arg.regionMatches(true, 0, searched, 0, searched.length())
                ).limit(MAX_TAB_RETURN).collect(Collectors.toList());
        }

        return new ArrayList<>();
    }

    private final class ReflectCommand extends Command {
        private AbstractCommand exe = null;

        protected ReflectCommand(String command) {
            super(command);
        }

        public void setExecutor(AbstractCommand exe) {
            this.exe = exe;
        }

        @Override
        public boolean execute(CommandSender sender, String commandLabel, String[] args) {
            if (exe != null) {
                return exe.onCommand(sender, this, commandLabel, args);
            }
            return false;
        }

        @Override
        public List<String> tabComplete(CommandSender sender, String alias, String[] args){
            return exe.onTabComplete(sender, this, alias, args);
        }
    }
}