package fr.heartgames.heartgamesapi.utils;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Toinetoine1 on 24/08/2019.
 */

public class GeoLocalisation {

    /*
    Merci de ne pas toucher à ceci !
     */
    private static final String KEY = "41f4e243a19414ea955112481e7e2e48&format";


    public static String getCountryName(String ip){
        String country = "null";
        try {
            HttpURLConnection request = (HttpURLConnection) new URL("http://api.ipstack.com/"+ip+"?access_key="+KEY+"&format=1").openConnection();
            request.connect();

            JsonParser jp = new JsonParser();
            JsonObject json = (JsonObject) jp.parse(new InputStreamReader(request.getInputStream()));
            country = json.get("country_name").getAsString();

            request.disconnect();
        } catch (Exception ignored) {}

        return country;
    }
}
