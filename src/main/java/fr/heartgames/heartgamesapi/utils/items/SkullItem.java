/*
 * Copyright (c) inventivetalent.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are
 *  permitted provided that the following conditions are met:
 *
 *     1. Redistributions of source code must retain the above copyright notice, this list of
 *        conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright notice, this list
 *        of conditions and the following disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 *  FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR
 *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 *  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  The views and conclusions contained in the software and documentation are those of the
 *  authors and contributors and should not be interpreted as representing official policies,
 *  either expressed or implied, of anybody else.
 */

package fr.heartgames.heartgamesapi.utils.items;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import lombok.*;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Data
@ToString(doNotUseGetters = true,
        callSuper = true)
@EqualsAndHashCode(doNotUseGetters = true,
        callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
public class SkullItem extends Item {

    private String owner;
    private String texture;

    @Override
    public ItemStack asStack() {
        ItemStack stack = super.asStack();
        if (stack.getItemMeta() != null) {
            SkullMeta meta = (SkullMeta) stack.getItemMeta();
            if(texture != null){
                stack.setType(Material.SKULL_ITEM);
                GameProfile profile = new GameProfile(UUID.randomUUID(), null);
                profile.getProperties().put("textures", new Property("textures", texture));
                Field profileField;
                try {
                    profileField = meta.getClass().getDeclaredField("profile");
                    profileField.setAccessible(true);
                    profileField.set(meta, profile);
                } catch (Exception e) {}
                stack.setItemMeta(meta);
                return stack;
            }

            meta.setOwner(this.owner);
            stack.setItemMeta(meta);
        }
        return stack;
    }

    public static SkullItem of(ItemStack itemStack) {
        Item item = Item.of(itemStack);
        if (itemStack.getItemMeta() != null) {
            ((SkullItem) item).setOwner(((SkullMeta) itemStack.getItemMeta()).getOwner());
        }
        return (SkullItem) item;
    }

    public static ItemStack createHeadByData(String data, int amount, String name,List<String> lore) {
        ItemStack item = new ItemStack(Material.SKULL_ITEM);
        item.setDurability((short) 3);
        item.setAmount(amount);
        ItemMeta meta = item.getItemMeta();
        if (lore != null && !lore.isEmpty()) {
            meta.setLore(lore);
        }
        if (!name.equals("")) {
            meta.setDisplayName(name.replace("&", "§"));
        }
        item.setItemMeta(meta);
        SkullMeta headMeta = (SkullMeta) item.getItemMeta();
        GameProfile profile = new GameProfile(UUID.randomUUID(), null);
        profile.getProperties().put("textures", new Property("textures", data));
        Field profileField;
        try {
            profileField = headMeta.getClass().getDeclaredField("profile");
            profileField.setAccessible(true);
            profileField.set(headMeta, profile);
        } catch (Exception e) {
        }
        item.setItemMeta(headMeta);
        return item;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder extends Item.Builder<SkullItem, Builder> {

        protected Builder() {
        }

        protected Builder(SkullItem item) {
            super(item);
        }

        @Override
        protected SkullItem init() {
            return new SkullItem();
        }

        @Override
        public SkullItem build() {
            return super.build();
        }

        public Builder owner(String owner) {
            item.owner = owner;
            return this;
        }

        public Builder texture(String texture){
            item.texture = texture;
            return this;
        }

    }

}
