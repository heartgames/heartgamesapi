package fr.heartgames.heartgamesapi.utils.reflections;

import org.bukkit.Bukkit;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Classe contenant plusieurs méthodes utiles pour l'utilisation de la
 * réflection. Pour la gestion des fields, préférez
 */
public class ReflectionUtils {
    /**
     * Récupčre la version Bukkit
     *
     * @return La version
     */
    public static String getBukkitVersion() {
        String name = Bukkit.getServer().getClass().getPackage().getName();
        return name.substring(name.lastIndexOf('.') + 1) + ".";
    }

    /**
     * Récupčre un constructeur
     *
     * @param clazz
     *            La classe contenenant le constructeur
     * @param args
     *            Les arguments du constructeur
     * @return Le constructeur
     */
    public static Constructor<?> getConstructor(Class<?> clazz, Class<?>... args) {
        try {
            return clazz.getConstructor(args);
        } catch (NoSuchMethodException | SecurityException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static List<Field> getAllModelFields(Class aClass) {
        List<Field> fields = new ArrayList<>();
        do {
            Collections.addAll(fields, aClass.getDeclaredFields());
            aClass = aClass.getSuperclass();
        } while (aClass != null);
        return fields;
    }

    /**
     * Récupčre et rend accessible un Field
     *
     * @param clazz
     *            Classe contenant le field
     * @param name
     *            Nom du field
     * @return Le Field
     */
    public static Field getField(Class<?> clazz, String name, boolean declared) {
        try {
            Field field = declared ? clazz.getDeclaredField(name) : clazz.getField(name);
            field.setAccessible(true);
            return field;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Récupčre la méthode getHandle présente sur beaucoup d'objets OBC
     *
     * @param obj
     *            L'object en question
     * @return Le résultat du getHandle()s
     */
    public static Object getHandle(Object obj) {
        try {
            return getMethod(obj.getClass(), "getHandle", new Class[0]).invoke(obj, new Object[0]);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Récupčre une méthode
     *
     * @param clazz
     *            La classe contenenant la méthode
     * @param name
     *            Le nom de la méthode
     * @param args
     *            Les arguments de la méthode
     * @return La méthode
     */
    public static Method getMethod(Class<?> clazz, String name, Class<?>... args) {
        try {
            return clazz.getMethod(name, args);
        } catch (NoSuchMethodException | SecurityException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Récupčre une classe NMS
     *
     * @param className
     *            Le nom de la classe recherchée
     * @return La classe trouvée
     */
    public static Class<?> getNMSClass(String className) {
        try {
            return Class.forName("net.minecraft.server." + getBukkitVersion() + className);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Récupčre une classe OBC (org.bukkit.craftbukkit)
     *
     * @param className
     *            Le nom de la classe ŕ trouver
     * @return La classe trouvée
     */
    public static Class<?> getOBCClass(String className) {
        try {
            return Class.forName("org.bukkit.craftbukkit." + getBukkitVersion() + className);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Récupčre une classe appartenant ŕ une autre
     *
     * @param clazz
     *            La classe "mčre"
     * @param name
     *            Le nom de la sous classe
     * @return La classe trouvée
     */
    public static Class<?> getSubClass(Class<?> clazz, String name) {
        for (Class<?> c : clazz.getDeclaredClasses()) {
            if (c.getSimpleName().equals("EnumTitleAction")) {
                return c;
            }
        }

        return null;
    }

    /**
     * Enlčve le flag 'final' ŕ un Field.
     *
     * @param field
     *            Le field auquel enlever le flag 'final'.
     */
    public static void removeFinal(Field field) {
        if (Modifier.isFinal(field.getModifiers())) {
            try {
                Reflector reflector = new Reflector(field);
                reflector.setFieldValue("modifiers", field.getModifiers() & 0xffffffef);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public enum PackageType {
        MINECRAFT_SERVER("net.minecraft.server." + getServerVersion()),
        CRAFTBUKKIT("org.bukkit.craftbukkit." + getServerVersion()),
        CRAFTBUKKIT_BLOCK(CRAFTBUKKIT, "block"),
        CRAFTBUKKIT_CHUNKIO(CRAFTBUKKIT, "chunkio"),
        CRAFTBUKKIT_COMMAND(CRAFTBUKKIT, "commands"),
        CRAFTBUKKIT_CONVERSATIONS(CRAFTBUKKIT, "conversations"),
        CRAFTBUKKIT_ENCHANTMENS(CRAFTBUKKIT, "enchantments"),
        CRAFTBUKKIT_ENTITY(CRAFTBUKKIT, "entity"),
        CRAFTBUKKIT_EVENT(CRAFTBUKKIT, "event"),
        CRAFTBUKKIT_GENERATOR(CRAFTBUKKIT, "generator"),
        CRAFTBUKKIT_HELP(CRAFTBUKKIT, "help"),
        CRAFTBUKKIT_INVENTORY(CRAFTBUKKIT, "inventory"),
        CRAFTBUKKIT_MAP(CRAFTBUKKIT, "map"),
        CRAFTBUKKIT_METADATA(CRAFTBUKKIT, "metadata"),
        CRAFTBUKKIT_POTION(CRAFTBUKKIT, "potion"),
        CRAFTBUKKIT_PROJECTILES(CRAFTBUKKIT, "projectiles"),
        CRAFTBUKKIT_SCHEDULER(CRAFTBUKKIT, "scheduler"),
        CRAFTBUKKIT_SCOREBOARD(CRAFTBUKKIT, "scoreboard"),
        CRAFTBUKKIT_UPDATER(CRAFTBUKKIT, "updater"),
        CRAFTBUKKIT_UTIL(CRAFTBUKKIT, "util");

        private final String path;

        /**
         * Construct a new package type
         *
         * @param path Path of the package
         */
        PackageType(String path) {
            this.path = path;
        }

        /**
         * Construct a new package type
         *
         * @param parent Parent package of the package
         * @param path   Path of the package
         */
        PackageType(PackageType parent, String path) {
            this(parent + "." + path);
        }

        /**
         * Returns the version of your server
         *
         * @return The server version
         */
        public static String getServerVersion() {
            return Bukkit.getServer().getClass().getPackage().getName().substring(23);
        }

        /**
         * Returns the path of this package type
         *
         * @return The path
         */
        public String getPath() {
            return path;
        }

        /**
         * Returns the class with the given name
         *
         * @param className Name of the desired class
         * @return The class with the specified name
         * @throws ClassNotFoundException If the desired class with the specified name and package cannot be found
         */
        public Class<?> getClass(String className) throws ClassNotFoundException {
            return Class.forName(this + "." + className);
        }

        // Override for convenience
        @Override
        public String toString() {
            return path;
        }
    }

    public static void setValue(Object instance, boolean declared, String fieldName, Object value) throws IllegalArgumentException, IllegalAccessException, SecurityException {
        setValue(instance, instance.getClass(), declared, fieldName, value);
    }

    public static void setValue(Object instance, Class<?> clazz, boolean declared, String fieldName, Object value) throws IllegalArgumentException, IllegalAccessException, SecurityException {
        getField(clazz, fieldName, declared).set(instance, value);
    }
}
