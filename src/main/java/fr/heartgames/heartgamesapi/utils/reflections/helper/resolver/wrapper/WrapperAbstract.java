package fr.heartgames.heartgamesapi.utils.reflections.helper.resolver.wrapper;

public abstract class WrapperAbstract {

	/**
	 * Check whether the wrapped object exists (i.e. is not null)
	 *
	 * @return <code>true</code> if the wrapped object exists
	 */
	public abstract boolean exists();

}
