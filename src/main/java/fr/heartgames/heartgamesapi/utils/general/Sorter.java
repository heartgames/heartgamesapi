package fr.heartgames.heartgamesapi.utils.general;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by Toinetoine1 on 29/09/2019.
 */

public class Sorter {

    public static <T> List<T> sort(Map<T, Long> map){
        return map.entrySet()
                .stream()
                .sorted(Comparator.comparingLong(Map.Entry::getValue))
                .map(Map.Entry::getKey)
                .limit(10)
                .collect(Collectors.toList());
    }

}
