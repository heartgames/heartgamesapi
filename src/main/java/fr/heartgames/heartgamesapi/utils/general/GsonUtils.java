package fr.heartgames.heartgamesapi.utils.general;

import com.google.common.base.Charsets;
import com.google.gson.*;
import fr.heartgames.heartgamesapi.utils.GsonFactory;
import lombok.Getter;

import java.io.*;
import java.lang.reflect.Type;

public class GsonUtils {

    @Getter
    private static Gson gson;
    @Getter
    private static Gson compactGson;

    static {
        gson = new GsonBuilder()
                .setPrettyPrinting()
                .disableHtmlEscaping()
                .serializeNulls()
                .create();

        compactGson = new GsonBuilder()
                .disableHtmlEscaping()
                .serializeNulls()
                .create();
    }

    public static <T> T convert(JsonElement element, Class<T> clazz) {
        return GsonFactory.getPrettyGson().fromJson(element, clazz);
    }

    private static InputStreamReader getInputStream(File file) throws UnsupportedEncodingException, FileNotFoundException {
        return new InputStreamReader(new FileInputStream(file), Charsets.UTF_8.name());
    }

    public static <T> T load(File file, Class<T> clazz) {
        try {
            if (!file.exists()) {
                FileUtils.save(file, "{}");
            }

            T t = gson.fromJson(getInputStream(file), clazz);
            if (t == null) {
                try {
                    t = clazz.newInstance();
                } catch (Exception var4) {
                    var4.printStackTrace();
                }

                save(file, t, true);
            }

            return t;
        } catch (JsonIOException | UnsupportedEncodingException | JsonSyntaxException var5) {
            var5.printStackTrace();
            return null;
        } catch (FileNotFoundException var6) {
            return null;
        }
    }

    public static <T> T load(File file, Type type, T def) {
        try {
            if (!file.exists()) {
                FileUtils.save(file, "{}");
            }

            T t = GsonFactory.getPrettyGson().fromJson(getInputStream(file), type);
            if (t == null) {
                t = def;
                save(file, def, true);
            }

            return t;
        } catch (JsonIOException | UnsupportedEncodingException | JsonSyntaxException var4) {
            var4.printStackTrace();
            return null;
        } catch (FileNotFoundException var5) {
            return null;
        }
    }

    public static JsonArray loadArray(File file) {
        if (!file.exists() || file.length() == 0L) {
            FileUtils.save(file, "[]");
        }

        try {
            JsonParser parser = new JsonParser();
            JsonElement jsonElement = parser.parse(getInputStream(file));
            return jsonElement.getAsJsonArray();
        } catch (FileNotFoundException var3) {
        } catch (UnsupportedEncodingException | JsonParseException var4) {
            var4.printStackTrace();
        }

        return new JsonArray();
    }

    public static JsonObject loadObject(File file) {
        if (!file.exists() || file.length() == 0L) {
            FileUtils.save(file, "{}");
        }

        try {
            JsonParser parser = new JsonParser();
            JsonElement jsonElement = parser.parse(getInputStream(file));
            return jsonElement.getAsJsonObject();
        } catch (UnsupportedEncodingException | JsonParseException var3) {
            var3.printStackTrace();
        } catch (FileNotFoundException var4) {
        }

        return new JsonObject();
    }

    public static void save(File file, JsonElement element, boolean indented) {
        String toSave = indented ? GsonFactory.getPrettyGson().toJson(element) : GsonFactory.getCompactGson().toJson(element);
        FileUtils.save(file, toSave);
    }

    public static void save(File file, Object object, boolean indented) {
        JsonElement element = GsonFactory.getPrettyGson().toJsonTree(object);
        System.out.println(element);
        save(file, element, indented);
    }
}
