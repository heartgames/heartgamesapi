package fr.heartgames.heartgamesapi.utils.general;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Toinetoine1 on 30/11/2019.
 */

public class StringUtils {

    public static String[] convertStringToArray(String text, char color){
        List<String> lines = new ArrayList<>();
        String[] array = text.split(" ");

        int wordPerLine = 7;
        int count = 0;
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < array.length; i++) {
            builder.append(array[i]).append(" ");
            count++;
            if(wordPerLine == count){
                lines.add("§"+color + builder.toString());
                builder.setLength(0);
                count = 0;
            }
        }

        return lines.toArray(new String[0]);
    }


}
