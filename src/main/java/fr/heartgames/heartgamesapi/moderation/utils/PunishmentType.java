package fr.heartgames.heartgamesapi.moderation.utils;

/**
 * Created by Toinetoine1 on 22/08/2019.
 */

public enum PunishmentType {

    BAN("Ban", null, false),
    BAN_IP("Ban-IP", BAN, false),
    TEMP_BAN("TempBan", BAN, true),
    TEMP_BAN_IP("TempBan-IP", BAN, true),
    MUTE("Mute", null, false),
    TEMP_MUTE("TempMute", MUTE, true),
    KICK("Kick", null, false);

    String type;
    PunishmentType from;
    boolean temp;

    PunishmentType(String type, PunishmentType from, boolean temp) {
        this.type = type;
        this.from = from;
        this.temp = temp;
    }

    public String getType() {
        return type;
    }

    public PunishmentType getFrom() {
        return from == null ? this : from;
    }

    public boolean isTemp() {
        return temp;
    }
}
