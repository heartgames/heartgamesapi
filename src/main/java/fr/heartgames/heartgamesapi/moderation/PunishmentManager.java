package fr.heartgames.heartgamesapi.moderation;

import fr.heartgames.heartgamesapi.moderation.utils.PunishmentType;
import fr.heartgames.heartgamesapi.moderation.utils.SQLModerationQuery;
import fr.heartgames.heartgamesapi.tech.sql.DatabaseAccess;
import fr.heartgames.heartgamesapi.tech.sql.SQLFactory;
import fr.heartgames.heartgamesapi.utils.general.TimeUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Toinetoine1 on 23/08/2019.
 */

public class PunishmentManager {

    private static PunishmentManager instance = null;
    private final Set<Punishment> punishments = Collections.synchronizedSet(new HashSet<>());
    private final Set<Punishment> history = Collections.synchronizedSet(new HashSet<>());
    private final Set<String> cached = Collections.synchronizedSet(new HashSet<>());

    public static PunishmentManager get() {
        return instance == null ? instance = new PunishmentManager() : instance;
    }

    public void setup() {
        try {
            SQLFactory.executeStatement(SQLModerationQuery.DELETE_OLD_PUNISHMENTS, TimeUtils.getTime());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void discard(String name) {
        cached.remove(name);

        Iterator<Punishment> iterator = punishments.iterator();
        while (iterator.hasNext()) {
            Punishment punishment = iterator.next();
            if (punishment.getName().equals(name)) {
                iterator.remove();
            }
        }

        iterator = history.iterator();
        while (iterator.hasNext()) {
            Punishment punishment = iterator.next();
            if (punishment.getName().equals(name)) {
                iterator.remove();
            }
        }
    }

    public List<Punishment> getPunishments(String name, PunishmentType punishmentType, boolean current) {
        List<Punishment> ptList = new ArrayList<>();

        if (isCached(name)) {
            System.out.println("get punishments by cache");
            for (Iterator<Punishment> iterator = (current ? punishments : history).iterator(); iterator.hasNext();) {
                Punishment pt = iterator.next();
                if ((punishmentType == null || punishmentType == pt.getType().getFrom()) && pt.getName().equals(name)) {
                    if (!current || !pt.isExpired()) {
                        ptList.add(pt);
                    } else {
                        pt.delete(false);
                        iterator.remove();
                    }
                }
            }
        } else {
            System.out.println("get punishments by sql");
            try {
                Connection connection = DatabaseAccess.getInstance().getConnection();
                PreparedStatement statement = SQLFactory.executeResultStatement(current ? SQLModerationQuery.SELECT_USER_PUNISHMENTS : SQLModerationQuery.SELECT_USER_PUNISHMENTS_HISTORY, connection, name);
                ResultSet rs = statement.executeQuery();
                while (rs.next()) {
                    Punishment punishment = getPunishmentFromResultSet(rs);
                    System.out.println("punishements find !");
                    System.out.println("type: "+(punishmentType == punishment.getType().getFrom()) +" punishmentType: "+punishmentType+" type: "+punishment.getType().getFrom());
                    System.out.println("current: "+current);
                    System.out.println("is expired:"+ punishment.isExpired());
                    System.out.println("1:"+(punishmentType == null || punishmentType == punishment.getType().getFrom()));
                    System.out.println("2:"+(!current || !punishment.isExpired()));
                    if ((punishmentType == null || punishmentType == punishment.getType().getFrom()) && (!current || !punishment.isExpired())) {
                        ptList.add(punishment);
                    }
                }
                rs.close();
                statement.close();
                connection.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }

        System.out.println(ptList);
        return ptList;
    }

    public List<Punishment> getPunishments(SQLModerationQuery sqlQuery, Object... parameters) {
        List<Punishment> ptList = new ArrayList<>();

        try {
            Connection connection = DatabaseAccess.getInstance().getConnection();
            PreparedStatement statement = SQLFactory.executeResultStatement(sqlQuery, connection, parameters);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                Punishment punishment = getPunishmentFromResultSet(rs);
                ptList.add(punishment);
            }
            rs.close();
            statement.close();
            connection.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return ptList;
    }

    public Punishment getPunishment(int id) {
        Punishment pt = null;
        try {
            Connection connection = DatabaseAccess.getInstance().getConnection();
            PreparedStatement statement = SQLFactory.executeResultStatement(SQLModerationQuery.SELECT_PUNISHMENT_BY_ID, connection, id);
            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                pt = getPunishmentFromResultSet(rs);
            }
            rs.close();
            statement.close();
            connection.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return pt == null || pt.isExpired() ? null : pt;
    }

    public InterimData load(String name) {
        Set<Punishment> punishments = new HashSet<>();
        Set<Punishment> history = new HashSet<>();
        try {
            Connection connection = DatabaseAccess.getInstance().getConnection();
            PreparedStatement statement = SQLFactory.executeResultStatement(SQLModerationQuery.SELECT_USER_PUNISHMENTS_WITH_IP, connection, name);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                punishments.add(getPunishmentFromResultSet(rs));
            }
            rs.close();
            statement.close();
            connection.close();

            connection = DatabaseAccess.getInstance().getConnection();
            statement = SQLFactory.executeResultStatement(SQLModerationQuery.SELECT_USER_PUNISHMENTS_HISTORY_WITH_IP, connection, name);
            rs = statement.executeQuery();
            while (rs.next()) {
                history.add(getPunishmentFromResultSet(rs));
            }
            rs.close();
            statement.close();
            connection.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        InterimData interimData = new InterimData(name, punishments, history);
        if(interimData.getBan() != null){
            interimData.accept();
        }

        return interimData;
    }

    public Punishment getBan(String name) {
        List<Punishment> punishments = getPunishments(name, PunishmentType.BAN, true);
        System.out.println("getBan punishments: "+punishments.stream().map(Punishment::getId).collect(Collectors.toList()));
        return punishments.isEmpty() ? null : punishments.get(0);
    }

    public Punishment getMute(String name) {
        List<Punishment> punishments = getPunishments(name, PunishmentType.MUTE, true);
        return punishments.isEmpty() ? null : punishments.get(0);
    }

    public boolean isBanned(String name) {
        return getBan(name) != null;
    }

    public boolean isCached(String name) {
        return cached.contains(name);
    }

    public void addCached(String name) {
        cached.add(name);
    }

    public boolean isMuted(String name) {
        return getMute(name) != null;
    }

    public Set<Punishment> getLoadedPunishments(boolean checkExpired) {
        if (checkExpired) {
            List<Punishment> toDelete = new ArrayList<>();
            for (Punishment pu : punishments) {
                if (pu.isExpired()) {
                    toDelete.add(pu);
                }
            }
            for (Punishment pu : toDelete) {
                pu.delete();
            }
        }
        return punishments;
    }

    public Set<Punishment> getLoadedHistory() {
        return history;
    }

    private Punishment getPunishmentFromResultSet(ResultSet rs) throws SQLException {
        return new Punishment(
                rs.getString("name"),
                rs.getString("operator"),
                rs.getString("reason"),
                rs.getLong("start"),
                rs.getLong("end"),
                PunishmentType.valueOf(rs.getString("punishmentType")),
                rs.getInt("id"));
    }

}
