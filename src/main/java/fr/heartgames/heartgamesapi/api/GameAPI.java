package fr.heartgames.heartgamesapi.api;

import fr.heartgames.heartgamesapi.api.players.HeartGamesPlayer;
import fr.heartgames.heartgamesapi.api.scoreboard.CustomObjective;
import fr.heartgames.heartgamesapi.run.RunType;
import fr.heartgames.heartgamesapi.servers.Server;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.List;

/**
 * Created by Toinetoine1 on 02/08/2019.
 */

public abstract class GameAPI extends JavaPlugin {

    protected static GameAPI gameAPI;

    public static GameAPI getAPI() {
        return gameAPI;
    }

    public abstract RunType getRunType();

    public abstract List<HeartGamesPlayer> getOnlinePlayers();

    public abstract Server getGameServer();

    public abstract void formatChat(boolean enabled);

    public abstract void enableTabList(boolean enabled);

    public abstract CustomObjective buildCustomObjective(HeartGamesPlayer player);
}
