package fr.heartgames.heartgamesapi.api.players.data;

import fr.heartgames.heartgamesapi.permissions.Rank;
import fr.heartgames.heartgamesapi.permissions.groups.Group;
import org.bson.codecs.pojo.annotations.BsonDiscriminator;

import java.util.List;

/**
 * Created by Toinetoine1 on 04/08/2019.
 */

@BsonDiscriminator()
public interface PlayerData {

    String getName();

    boolean isOnlineMode();

    void setPassword(String pass);

    String getPassword();

    boolean hasPassword();

    void setOnlineMode(boolean b);

    void setName(String name);

    Rank getMainRank();

    void setMainRank(Rank rank);

    void addHeartcoins(int heartcoins);

    int getHeartcoins();

    void removeHeartcoins(int heartcoins);

    long getFirstJoin();

    void setFirstJoin(long firstJoin);

    long getLastJoin();

    void setLastJoin(long lastJoin);

    List<String> getPermissions();

    boolean hasPermission(String perm);

    List<Group> getRealSubGroup();

    List<String> getSubGroup();

    boolean addSubGroup(Rank rank);

    boolean removeSubGroup(Rank rank);

    boolean hasSubGroup(Rank rank);

    void saveGameData();

    List<String> getKnownIp();

    List<String> getKnownNames();
}
